<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
class UnitTestData extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$check1 = DB::table('oauth_clients')->where('secret', 'FbLb8joUs4Jon329CMfdafxWNEXFh4IIBAI4efR6')
    										->get()->toArray();
    	if (count($check1) == 0) {
    		DB::table('oauth_clients')->insert([
	  			'id' => 1,
	  			'name' => 'Lumen Password Grant Client',
	  			'secret' => 'FbLb8joUs4Jon329CMfdafxWNEXFh4IIBAI4efR6',
	  			'provider' => 'users',
	  			'redirect' => 'http://localhost',
	  			'personal_access_client' => 0,
	  			'password_client' => 1,
	  			'revoked' => 0,
	  			'created_at' => '2020-07-20 12:43:53',
	  			'updated_at' => '2020-07-20 12:43:53'
	  		]);
    	}

    	$check2 = DB::table('users')->where('email', 'ut_12345@gmail.com')
    										->get()->toArray();
  		if(count($check2) == 0){
  			DB::table('users')->insert([
	  			'id' => '1',
	  			'name' => 'name testing',
	  			'password' => Hash::make('11111111'),
	  			'email' => 'ut_12345@gmail.com',
	  			'created_at' => date('Y-m-d H:i:s'),
	  			'updated_at' => date('Y-m-d H:i:s')
	  		]);
  		}

  		$check3 = DB::table('companies')->where('id', 1)
    										->get()->toArray();
  		if(count($check3) == 0){
  			DB::table('companies')->insert([
	  			'id' => '1',
	  			'user_id' => '1',
	  			'name' => 'unit test company name',
	  			'description' => 'unit test description',
	  			'created_at' => date('Y-m-d H:i:s'),
	  			'updated_at' => date('Y-m-d H:i:s')
	  		]);
  		}
    }
}
