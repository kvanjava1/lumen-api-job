<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
$router->get('/', function ()  use ($router){
    return "test job";
});

$router->group(['prefix' => '/user'], function () use ($router) {
   $router->post('/register','UserController@register');
});

$router->group(['prefix' => 'company', 'middleware' => 'auth:api'], function () use ($router) {
    $router->post('/add','CompanyController@add');
    $router->get('/mycompany/list','CompanyController@getMyCompanyList');
});

$router->get('/job','JobController@showAllJob');
$router->group(['prefix' => 'job', 'middleware' => 'auth:api'], function () use ($router) {
	$router->post('/add','JobController@add');
	$router->get('/myjob/list','JobController@getMyJobList');
	$router->get('{jobId}/aplicants/list','JobController@getApplicantsList');

	$router->post('/apply','JobController@apply');
	$router->get('/apply/history','JobController@apply');
});