<?php

namespace App\Http\Helpers;

use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;

class ResponseHelper
{
    public function success(array $payload, string $message, int $httpCode): JsonResponse {
        return response()->json([
                'success' => true,
                'message' => $message,
                'result' => $payload
            ], $httpCode
        );
    }  

    public function failed(array $errors, string $message, int $httpCode): JsonResponse {
        return response()->json([
                'success' => false,
                'message' => $message, 
                'errors' => $errors
            ], $httpCode
        );
    }  
}