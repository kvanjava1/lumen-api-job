<?php

namespace App\Http\Controllers;

use App\Http\Services\UserService;
use App\Http\Helpers\ResponseHelper;

use Illuminate\Http\Request;
use Validator;

class UserController extends Controller
{
    private $responseHelper;
    private $userService;

    function __construct(){
        $this->responseHelper = new ResponseHelper;
        $this->userService = new UserService;
    }

    public function register(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required'
        ]);

        if($validator->fails()){
            return $this->responseHelper->failed(
                $validator->errors()->toArray(), "failed register user", 422
            );
        }

        $register = $this->userService->setInput($request->all())
                                      ->register();

        if($register->getResult() == false){
            return $this->responseHelper->failed(
                [], $register->getErrorMessage(), 422
            );
        }

        return $this->responseHelper->success(
            [], "register user success", 200
        );
    }  
     
}