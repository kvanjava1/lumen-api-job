<?php

namespace App\Http\Controllers;

use App\Http\Services\CompanyService;
use App\Http\Helpers\ResponseHelper;

use App\Company;

use Illuminate\Http\Request;
use Validator, Auth;

class CompanyController extends Controller
{
    private $responseHelper;
    private $companyService;
    private $mdlCompany;

    function __construct(){
        $this->companyService = new CompanyService; 
        $this->responseHelper = new ResponseHelper;
        $this->mdlCompany = new company;
    }

    public function add(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:companies|max:255',
            'description' => 'required|max:1000',
        ]);

        if($validator->fails()){
            return $this->responseHelper->failed(
                $validator->errors()->toArray(), "failed register company", 422
            );
        }

        $this->companyService->setInput($request->all())
                                    ->add();

        return $this->responseHelper->success(
            [], "register company success", 200
        );
    }  
    
    public function getMyCompanyList(Request $request){
        $myCompanyList = $this->mdlCompany->where('user_id', Auth::guard('api')->user()->id)
                                          ->paginate(5);

        return $this->responseHelper->success(
            $myCompanyList->toArray(), "get company list success", 200
        );
    }
}