<?php

namespace App\Http\Controllers;

use App\Http\Services\JobService;
use App\Http\Helpers\ResponseHelper;

use App\Company;
use App\Job;
use App\JobApplication;

use Illuminate\Http\Request;
use Validator, Auth;

class JobController extends Controller
{
    private $responseHelper;
    private $jobService;
    private $mdlCompany;
    private $mdlJob;
    private $mdljobApplication;

    function __construct(){
        $this->responseHelper = new ResponseHelper;
        $this->jobService = new JobService;
        $this->mdlCompany = new company;
        $this->mdlJob = new Job;
        $this->mdljobApplication = new JobApplication;
    }

    public function add(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'description' => 'required|max:1000',
            'company_id' => 'required|integer',
            'is_published' => 'required|integer',
        ]);

        if($validator->fails()){
            return $this->responseHelper->failed(
                $validator->errors()->toArray(), "failed register job", 422
            );
        }

        $company = $this->mdlCompany->where('id', $request->get('company_id'))
                                    ->where('user_id', Auth::guard('api')->user()->id)
                                    ->first();

        if ($company == null) {
            return $this->responseHelper->failed(
                ['company_id' => 'company not found'], "failed register job", 422
            );
        }

        $this->jobService->setCompany($company)
                         ->setInput($request->all())
                         ->add();

        return $this->responseHelper->success(
            [], "register job success", 200
        );
    }  
    
    public function getMyJobList(){
        $myJobList = $this->mdlJob->with('company')
                                  ->withCount('totalJobApplicants')
                                  ->where('user_id', Auth::guard('api')->user()->id)
                                  ->paginate(5);

        return $this->responseHelper->success(
            $myJobList->toArray(), "get my job list success", 200
        );
    }

    public function showAllJob(){
        $myJobList = $this->mdlJob->with('company')->withCount('totalJobApplicants')->where('is_published', 1)
                                  ->paginate(5);

        return $this->responseHelper->success(
            $myJobList->toArray(), "get job list success", 200
        );
    }

    public function apply(Request $request){
        $validator = Validator::make($request->all(), [
            'job_id' => 'required|integer',
            'note' => 'required|max:1000',
        ]);

        if($validator->fails()){
            return $this->responseHelper->failed(
                $validator->errors()->toArray(), "failed apply job", 422
            );
        }

        $job = $this->mdlJob->where('id', $request->get('job_id'))
                            ->where('is_published', 1)
                            ->where('user_id', Auth::guard('api')->user()->id)
                            ->first();

        if($job == null) {
            return $this->responseHelper->failed(
                ['job_id' => 'job not found'], "failed apply job", 422
            );
        }

        $checkAplicant = $this->mdljobApplication->where('user_id', Auth::guard('api')->user()->id)
                                                 ->where('job_id', $request->get('job_id'))
                                                 ->first();

        if($checkAplicant != null){
            return $this->responseHelper->failed(
                ['user_id' => 'this user already apply this job'], "failed apply job", 422
            );
        }

        $this->jobService->setInput($request->all())
                         ->setJob($job)
                         ->apply();
        
        return $this->responseHelper->success(
            [], "apply job success", 200
        );
    }

    public function getApplicantsList($jobId){
        $applicants = $this->mdljobApplication->with(['user', 'job', 'job.company'])
                                            ->where('job_id', $jobId)
                                            ->whereHas('job', function ($query) {
                                                return $query->where('user_id', Auth::guard('api')->user()->id);
                                            })->paginate(5);
        
        return $this->responseHelper->success(
            $applicants->toArray(), "get applicant job success", 200
        );
    }
}