<?php

namespace App\Http\Services;

use Illuminate\Support\Facades\Hash;

use App\Job;
use App\Company;
use App\JobApplication;

use DB;
use Auth;

class JobService
{
	private $mdlJob;
    private $mdlJobApplication;
    private $company;
    private $job;
	private $input;

	function __construct() {
        $this->mdlJob = new Job;
        $this->mdlCompany =  new Company;
        $this->mdlJobApplication = new JobApplication;
    }

    public function setCompany(object $company): JobService{
        $this->company = $company;
        return $this;
    }

    public function setInput(array $data): JobService{
    	$this->input = $data;
    	return $this;
    }

    public function setJob(object $job): JobService{
        $this->job = $job;
        return $this;
    }

    public function add(): Job {
        $this->mdlJob->user_id = Auth::guard('api')->user()->id;
    	$this->mdlJob->name = $this->input['name'];
    	$this->mdlJob->description = $this->input['description'];
        $this->mdlJob->company_id = $this->company->id;
        $this->mdlJob->is_published = $this->input['is_published'];
    	$this->mdlJob->save();

        return $this->mdlJob;
    }  

    public function apply(): JobApplication{
        $this->mdlJobApplication->job_id  = $this->job->id;
        $this->mdlJobApplication->user_id = Auth::guard('api')->user()->id;
        $this->mdlJobApplication->note = $this->input['note'];
        $this->mdlJobApplication->save();

        return $this->mdlJobApplication;
    }
     
}