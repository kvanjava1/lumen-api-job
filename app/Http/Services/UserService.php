<?php

namespace App\Http\Services;

use Illuminate\Support\Facades\Hash;
use App\User;
use DB;

class UserService
{
	private $mdlUser;
	private $input;

    private $result;
    private $errorCode;
    private $errorMessage;

	function __construct() {
        $this->mdlUser = new User;
    }

    public function setInput(array $data) : UserService{
    	$this->input = $data;
    	return $this;
    }

    public function register(): UserService {
    	$this->mdlUser->name = $this->input['name'];
    	$this->mdlUser->password = Hash::make($this->input['password']);
    	$this->mdlUser->email = $this->input['email'];
    	$this->mdlUser->save();

        return $this->setResult($this->mdlUser, 0, 'register success');
    }  

    private function setResult($result, int $errorCode, string $errorMessage): UserService{
        $this->result = $result;
        $this->errorCode = $errorCode;
        $this->errorMessage = $errorMessage;
        
        return $this;
    }

    public function getResult(){
        return $this->result;
    }

    public function getErrorCode(){
        return $this->errorCode;
    }
     
    public function getErrorMessage(){
        return $this->errorMessage;
    }
}