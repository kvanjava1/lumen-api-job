<?php

namespace App\Http\Services;

use Illuminate\Support\Facades\Hash;
use App\Company;
use DB;
use Auth;

class CompanyService
{
	private $mdlCompany;
	private $input;

	function __construct() {
        $this->mdlCompany = new Company;
    }

    public function setInput(array $data): CompanyService{
    	$this->input = $data;
    	return $this;
    }

    public function add(): Company {
        $this->mdlCompany->user_id = Auth::guard('api')->user()->id;
    	$this->mdlCompany->name = $this->input['name'];
    	$this->mdlCompany->description = $this->input['description'];
    	$this->mdlCompany->save();

        return $this->mdlCompany;
    }  
     
}