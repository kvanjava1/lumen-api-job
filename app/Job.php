<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    protected $table = 'jobs';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description', 'user_id', 'company_id', 'is_published'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    public function company(){
        return $this->belongsTo('App\Company', 'company_id', 'id');
    }

    public function jobApplication(){
        return $this->hasMany('App\JobApplication', 'job_id', 'id');
    }

    public function totalJobApplicants(){
        return $this->hasMany('App\JobApplication', 'job_id', 'id');
    }
}