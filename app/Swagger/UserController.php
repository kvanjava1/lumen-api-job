<?php
/**
* @OA\Get(
*     path="/job",
*     operationId="/job",
*     tags={"yourtag"},
*     @OA\Parameter(
*         name="category",
*         in="path",
*         description="The category parameter in path",
*         required=false,
*         @OA\Schema(type="string")
*     ),
*     @OA\Parameter(
*         name="criteria",
*         in="query",
*         description="Some optional other parameter",
*         required=false,
*         @OA\Schema(type="string")
*     ),
*     @OA\Response(
*         response="200",
*         description="Returns some sample category things",
*         @OA\JsonContent()
*     ),
*     @OA\Response(
*         response="400",
*         description="Error: Bad request. When required parameters were not supplied.",
*     ),
* )
*/

