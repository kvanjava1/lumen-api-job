<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class ApiTest extends TestCase
{
    /** @test */
    public function register()
    {
        $email = 'tesmail_' . date('ymdhis') . '@gmail.com';

        $this->json('POST', 'user/register', [
            'name' => 'tes',
            'email' => $email,
            'password' => '11111111'
        ], [
            'content_type' => 'application/json',
        ])->seeJsonStructure([
                'success',
                'message',
                'result'
        ]);

       $this->response->assertStatus(200);
    }

    /** @test */
    public function AuthPassport()
    {
        $this->json('POST', '/oauth/token/', [
            'username' => env('UT_EMAIL'),
            'grant_type' => 'password',
            'password' => env('UT_PASSWORD'),
            'client_id' => env('UT_ID'),
            'client_secret' => env('UT_SECRET'),
            'scope' => ''
        ])->seeJsonStructure([
            'token_type' ,
            'expires_in',
            'access_token',
            'refresh_token'
        ]);

        $this->response->assertStatus(200);
    }

    /** @test */
    public function addCompany(){

        $this->json('POST', '/oauth/token/', [
            'username' => env('UT_EMAIL'),
            'grant_type' => 'password',
            'password' => env('UT_PASSWORD'),
            'client_id' => env('UT_ID'),
            'client_secret' => env('UT_SECRET'),
            'scope' => ''
        ]);
     
        $content = json_decode($this->response->getContent());
        $accessToken = $content->token_type . ' ' . $content->access_token;

        $this->json('POST', 'company/add', [
            'name' => 'test name' . rand(1, 999),
            'description' => 'test ' . rand(1, 999)
        ],[
            'content_type' => 'application/json',
            'authorization' => $accessToken
        ])
        ->seeJsonStructure([
            'success',
            'message',
            'result'
        ]);
        
        $this->response->assertStatus(200);
    }

     /** @test */
    public function myCompanyList(){
        $this->json('POST', '/oauth/token/', [
            'username' => env('UT_EMAIL'),
            'grant_type' => 'password',
            'password' => env('UT_PASSWORD'),
            'client_id' => env('UT_ID'),
            'client_secret' => env('UT_SECRET'),
            'scope' => ''
        ]);
     
        $content = json_decode($this->response->getContent());
        $accessToken = $content->token_type . ' ' . $content->access_token;

        $this->json('GET', 'company/mycompany/list', [],[
            'content_type' => 'application/json',
            'authorization' => $accessToken
        ])
        ->seeJsonStructure([
            'success',
            'message',
            'result' => [
                'current_page',
                'data' => [
                            '*' =>[
                                    'id',
                                    'user_id',
                                    'name',
                                    'description',
                                    'created_at',
                                    'updated_at'        
                            ]
                          ],
                'first_page_url',
                'from',
                'last_page',
                'last_page_url',
                'next_page_url',
                'path',
                'per_page',
                'prev_page_url',
                'to',
                'total'
            ]
        ]);

        $this->response->assertStatus(200);
    }

    /** @test */
    public function addJob(){

        $this->json('POST', '/oauth/token/', [
            'username' => env('UT_EMAIL'),
            'grant_type' => 'password',
            'password' => env('UT_PASSWORD'),
            'client_id' => env('UT_ID'),
            'client_secret' => env('UT_SECRET'),
            'scope' => ''
        ]);
     
        $content = json_decode($this->response->getContent());
        $accessToken = $content->token_type . ' ' . $content->access_token;

        $this->json('POST', 'job/add', [
            'name' => 'job name' . rand(1, 999),
            'description' => 'job desc test ' . rand(1, 999),
            'is_published' => 1,
            'company_id' => 1
        ],[
            'content_type' => 'application/json',
            'authorization' => $accessToken
        ])
        ->seeJsonStructure([
            'success',
            'message',
            'result'
        ]);
        
        $this->response->assertStatus(200);
    }

    /** @test */
    public function myJobList(){

        $this->json('POST', '/oauth/token/', [
            'username' => env('UT_EMAIL'),
            'grant_type' => 'password',
            'password' => env('UT_PASSWORD'),
            'client_id' => env('UT_ID'),
            'client_secret' => env('UT_SECRET'),
            'scope' => ''
        ]);
     
        $content = json_decode($this->response->getContent());
        $accessToken = $content->token_type . ' ' . $content->access_token;

        $this->json('GET', 'job/myjob/list', [],[
            'content_type' => 'application/json',
            'authorization' => $accessToken
        ])
        ->seeJsonStructure([
            'success',
            'message',
            'result' => [
                'current_page',
                'data' => [
                            '*' =>[
                                    'id',
                                    'user_id',
                                    'name',
                                    'description',
                                    'is_published',
                                    'total_job_applicants_count',
                                    'company'=> [
                                        'id',
                                        'user_id',
                                        'name',
                                        'description',
                                        'created_at',
                                        'updated_at'
                                    ],
                                    'created_at',
                                    'updated_at',
                                    
                            ]
                ],
                'first_page_url',
                'from',
                'last_page',
                'last_page_url',
                'next_page_url',
                'path',
                'per_page',
                'prev_page_url',
                'to',
                'total'
            ]
        ]);
        
        $this->response->assertStatus(200);
    }

    /** @test */
    public function applyJob(){

        $this->json('POST', '/oauth/token/', [
            'username' => env('UT_EMAIL'),
            'grant_type' => 'password',
            'password' => env('UT_PASSWORD'),
            'client_id' => env('UT_ID'),
            'client_secret' => env('UT_SECRET'),
            'scope' => ''
        ]);
     
        $content = json_decode($this->response->getContent());
        $accessToken = $content->token_type . ' ' . $content->access_token;

        \DB::table('job_applications')->where('job_id', 1)
                                    ->where('user_id', 1)
                                    ->delete();

        $this->json('POST', 'job/apply', [
            'job_id' => 1,
            'note' => 'test note apply job' . rand(1, 999)
        ],[
            'content_type' => 'application/json',
            'authorization' => $accessToken
        ])
        ->seeJsonStructure([
            'success',
            'message',
            'result'
        ]);
        
        $this->response->assertStatus(200);
    }

    /** @test */
    public function job(){

        $this->json('POST', '/oauth/token/', [
            'username' => env('UT_EMAIL'),
            'grant_type' => 'password',
            'password' => env('UT_PASSWORD'),
            'client_id' => env('UT_ID'),
            'client_secret' => env('UT_SECRET'),
            'scope' => ''
        ]);
     
        $content = json_decode($this->response->getContent());
        $accessToken = $content->token_type . ' ' . $content->access_token;

        $this->json('GET', 'job', [],[
            'content_type' => 'application/json',
            'authorization' => $accessToken
        ])
        ->seeJsonStructure([
            'success',
            'message',
            'result' => [
                'current_page',
                'data' => [
                            '*' =>[
                                    'id',
                                    'user_id',
                                    'name',
                                    'description',
                                    'is_published',
                                    'total_job_applicants_count',
                                    'company'=> [
                                        'id',
                                        'user_id',
                                        'name',
                                        'description',
                                        'created_at',
                                        'updated_at'
                                    ],
                                    'created_at',
                                    'updated_at',
                                    
                            ]
                ],
                'first_page_url',
                'from',
                'last_page',
                'last_page_url',
                'next_page_url',
                'path',
                'per_page',
                'prev_page_url',
                'to',
                'total'
            ]
        ]);
        
        $this->response->assertStatus(200);
    }

    /** @test */
    public function jobApplicantsList(){

        $this->json('POST', '/oauth/token/', [
            'username' => env('UT_EMAIL'),
            'grant_type' => 'password',
            'password' => env('UT_PASSWORD'),
            'client_id' => env('UT_ID'),
            'client_secret' => env('UT_SECRET'),
            'scope' => ''
        ]);
     
        $content = json_decode($this->response->getContent());
        $accessToken = $content->token_type . ' ' . $content->access_token;

        $this->json('GET', 'job/1/aplicants/list', [],[
            'content_type' => 'application/json',
            'authorization' => $accessToken
        ])
        ->seeJsonStructure([
            'success',
            'message',
            'result' => [
                'current_page',
                'data' => [
                            '*' =>[
                                    'id',
                                    'user_id',
                                    'job_id',
                                    'note',
                                    'created_at',
                                    'updated_at',
                                    'user' => [
                                        'id',
                                        'name',
                                        'email',
                                        'created_at',
                                        'updated_at'
                                    ]                                   
                            ]
                ],
                'first_page_url',
                'from',
                'last_page',
                'last_page_url',
                'next_page_url',
                'path',
                'per_page',
                'prev_page_url',
                'to',
                'total'
            ]
        ]);
        
        $this->response->assertStatus(200);
    }
}
